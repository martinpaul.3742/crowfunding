<?php require_once 'parts/header.php'; ?>

<div class="container">
    <h1 class="text-center">Connexion</h1>

    <form method="POST" class="my-3">
        <div class="form-group">
            <div class="<?= (isset($errors['global'])) ? 'is-invalid' : '' ?>"></div>
            <div class="invalid-feedback"><?= $errors['global'] ?></div>
        </div>
        <div class="form-group">
            <label>Addresse mail</label>
            <input type="email" name="email" class="form-control <?= (isset($errors['email'])) ? 'is-invalid' : '' ?>" placeholder="Votre adresse mail">
            <div class="invalid-feedback"><?= $errors['email'] ?></div>
        </div>
        <div class="form-group">
            <label>Mot de passe</label>
            <input type="password" name="password" class="form-control" placeholder="Votre mot de passe">
        </div>
        <button class="btn btn-primary mr-3" type="submit">Se connecter</button>
        <a href="./register">S'inscrire</a>
    </form>
</div>

<?php require_once 'parts/footer.php'; ?>