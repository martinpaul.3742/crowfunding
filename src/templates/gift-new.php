<?php require_once 'parts/header.php'; ?>

<!-- CSS -->
<link rel="stylesheet" href="css/gift-new.css">

<div class="container">
    <h1><?= $project->getName() ?></h1>
    <p class="mb-4"><?= $project->getDateCreation()->format('d-m-Y') ?></p>
    <p><?= $project->getDescription() ?></p>
    <p class="author">Par <a href="./profile?user_id=<?= $project->getUserId() ?>"><?= $project->getAuthor()->getFullname() ?></a></p>

    <p class="d-flex align-items-center mt-5">Argent récolté : <span class="gifts d-flex align-items-start"><?= $project->getSumOfGifts() ?>€ <span>/ <?= $project->getGoal() ?>€</span></span></p>

    <?php if ($u->getBalance() == 0) { ?>
        <p>Argent disponible : <?= $u->getBalance() ?>€</p>
        <div class="alert alert-dismissible alert-danger">
            <strong>Vous n'avez plus d'argent !</strong> <a href="./my-profile" class="alert-link">Vous pouvez remettre des crédits</a> puis essayer à nouveau.
        </div>
    <?php } else if ($project->getRest() == 0) { ?>
        <div class="alert alert-dismissible alert-success">
            <strong>L'objectif est déjà atteint !</strong> <a href="./" class="alert-link">Vous pouvez chercher d'autre projets</a> qui pourraient vous intéresser.
        </div>
    <?php } else { ?>
        <div id="container-form">
            <p class="goal m-0">Il reste <span><?= $project->getRest() ?>€</span> pour que le projet atteigne son but</p>
            <p class="balance">→ Argent disponible : <?= $u->getBalance() ?>€</p>
            <form method="POST" class="my-3">
                <div class="form-group">
                    <label>Montant du don</label>
                    <div class="input-group mb-3  <?= (isset($errors['amount'])) ? 'is-invalid' : '' ?>">
                        <div class="input-group-prepend">
                            <span class="input-group-text">€</span>
                        </div>
                        <input type="number" name="amount" value="<?= $values['amount'] ?>" class="form-control" min="1" max="<?= $project->getRest() ?>">
                        <div class="input-group-append">
                            <span class="input-group-text">.00</span>
                        </div>
                    </div>
                    <div class="invalid-feedback"><?= $errors['amount'] ?></div>
                </div>
                <button class="btn btn-success" type="submit">Valider le montant</button>
            </form>
        </div>
    <?php } ?>
</div>

<?php require_once 'parts/footer.php'; ?>