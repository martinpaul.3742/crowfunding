<?php require_once 'parts/header.php'; ?>

<!-- CSS -->
<link rel="stylesheet" href="css/profile.css">

<div class="container">
    <div class="row">
        <div class="col-4 text-center">
            <div class="sidebar">
                <i class="avatar bi bi-person-fill"></i>
                <p class="top-informations">
                    <?= $user->getPseudo() ?><br/>
                    Enregistré le <?php echo (new Datetime($user->getDateRegistration()))->format('d-m-Y') ?><br/><br/>
                    <?= $user->getFullname() ?><br/>
                </p>
            </div>
        </div>
        <div class="col-8">
            <div id="projects">
                <h2>Projets</h2>
                <div class="row">
                    <?php
                        if ($projects) {
                            foreach ($projects as $project) {
                                echo '<div class="col-6">';
                                include('parts/card-project.php');
                                echo '</div>';
                            } 
                        } else {
                            echo '<p class="col-12">' . $user->getFullname() . ' n\'a pas encore de projet</p>';
                        }
                    ?>
                </div>
            </div>

            <div id="gifts">
                <h2>Dons</h2>
                <div class="row">
                    <?php 
                        if (count($gifts) == 0) {
                            echo '<p class="col-12">' . $user->getFullname() . ' n\'a pas encore fait de dons</p>';
                        } else {
                            echo (count($gifts) == 1) ? '<div class="container-card-gift once">' : '<div class="container-card-gift">';
                                foreach ($gifts as $gift) {
                                    include('parts/card-gift.php');
                                }
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php require_once 'parts/footer.php'; ?>

<!-- SCRIPT -->
<script src="js/profile.js"></script>