<?php require_once 'parts/header.php'; ?>

<!-- CSS -->
<link rel="stylesheet" href="css/my-profile.css">

<div class="container">
    <div class="row">
        <div class="col-4 text-center">
            <div class="sidebar">
                <i class="avatar bi bi-person-fill"></i>
                <p class="top-informations">
                    <?= $u->getPseudo() ?><br/>
                    Enregistré le <?php echo (new Datetime($u->getDateRegistration()))->format('d-m-Y') ?><br/><br/>
                    <?= $u->getEmail() ?><br/>
                </p>
                <a href="#update-profile" id="button-update-profile" class="btn mb-4">Modifier mes informations</a>
                <div>
                    <i class="balance-icon bi bi-credit-card-fill"></i>
                    <p class="balance">Balance : <?php echo $u->getBalance() . '€' ?><br/></p>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div id="update-profile">
                <div id="update-informations">
                    <h2>Informations personnelles</h2>
                    <form method="POST">
                        <div class="form-group">
                            <label>Pseudo</label>
                            <input type="text" name="pseudo" value="<?= $values['pseudo'] ?>" class="form-control <?= (isset($errors['pseudo'])) ? 'is-invalid' : '' ?>" placeholder="Votre pseudo">
                            <div class="invalid-feedback"><?= $errors['pseudo'] ?></div>
                        </div>
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" name="firstname" value="<?= $values['firstname'] ?>" class="form-control <?= (isset($errors['firstname'])) ? 'is-invalid' : '' ?>" placeholder="Votre prénom">
                            <div class="invalid-feedback"><?= $errors['firstname'] ?></div>
                        </div>
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" name="lastname" value="<?= $values['lastname'] ?>" class="form-control <?= (isset($errors['lastname'])) ? 'is-invalid' : '' ?>" placeholder="Votre nom">
                            <div class="invalid-feedback"><?= $errors['lastname'] ?></div>
                        </div>
                        <div class="form-group">
                            <label>Addresse mail</label>
                            <input type="email" name="email" value="<?= $values['email'] ?>" class="form-control <?= (isset($errors['email'])) ? 'is-invalid' : '' ?>" placeholder="Votre adresse mail">
                            <div class="invalid-feedback"><?= $errors['email'] ?></div>
                        </div>
                        <p class="mt-4">
                            <button class="btn btn-primary mr-3" type="submit">Modifier mes informations</button>
                            <a href="#" id="button-cancel-update-profile">Annuler</a>
                        </p>
                    </form>
                    <hr>
                </div>
                <div id="update-password">
                    <h2>Modification du mot de passe</h2>
                    <form method="POST" class="mb-4">
                        <div class="form-group">
                            <label>Mot de passe actuel</label>
                            <input type="password" name="password_old" class="form-control <?= (isset($errorsPassword['password_old'])) ? 'is-invalid' : '' ?>" placeholder="Votre ancien mot de passe">
                            <div class="invalid-feedback"><?= $errorsPassword['password_old'] ?></div>
                        </div>
                        <div class="form-group">
                            <label>Nouveau mot de passe</label>
                            <input type="password" name="password_new" class="form-control <?= (isset($errorsPassword['password_new'])) ? 'is-invalid' : '' ?>" placeholder="Votre nouveau mot de passe">
                            <div class="invalid-feedback"><?= $errorsPassword['password_new'] ?></div>
                        </div>
                        <div class="form-group">
                            <label>Confirmation du nouveau mot de passe</label>
                            <input type="password" name="password_new_repeated" class="form-control" placeholder="Confirmation de votre nouveau mot de passe">
                        </div>
                        <p class="mt-4">
                            <button class="btn btn-primary mr-3" type="submit">Modifier mon mot de passe</button>
                            <a href="#" id="button-cancel-update-password">Annuler</a>
                        </p>
                    </form>
                </div>
                <a href="#update-password" id="button-update-password" class="mr-3">Modifier mon mot de passe</a>
            </div>

            <div id="projects">
                <h2>Mes projets</h2>
                <div class="row">
                    <?php
                        if ($projects) {
                            foreach ($projects as $project) {
                                echo '<div class="col-6">';
                                include('parts/card-project.php');
                                echo '</div>';
                            } 
                        } else {
                            echo '<p class="col-12">Vous n\'avez pas encore de projet</p>';
                        }
                    ?>
                    <div class="col-6"><?php include('parts/card-add-project.php'); ?></div>
                </div>
            </div>

            <div id="gifts">
                <h2>Mes dons</h2>
                <?php 
                    if (count($gifts) == 0) {
                        echo '<p class="col-12">Vous n\'avez pas encore fait de dons</p>';
                    } else {
                        echo (count($gifts) == 1) ? '<div class="container-card-gift once">' : '<div class="container-card-gift">' ;
                            foreach ($gifts as $gift) {
                                include('parts/card-gift.php');
                            }
                        echo '</div>';
                    }
                ?>
            </div>
        </div>
    </div>

</div>

<?php require_once 'parts/footer.php'; ?>

<!-- SCRIPT -->
<script src="js/my-profile.js"></script>