<?php require_once 'parts/header.php'; ?>

<div class="container">
    <h1 class="text-center">Inscription</h1>

    <form method="POST" class="my-3">
        <div class="form-group">
            <label>Pseudo</label>
            <input type="text" name="pseudo" value="<?= $values['pseudo'] ?>" class="form-control <?= (isset($errors['pseudo'])) ? 'is-invalid' : '' ?>" placeholder="Votre pseudo">
            <div class="invalid-feedback"><?= $errors['pseudo'] ?></div>
        </div>
        <div class="form-group">
            <label>Prénom</label>
            <input type="text" name="firstname" value="<?= $values['firstname'] ?>" class="form-control <?= (isset($errors['firstname'])) ? 'is-invalid' : '' ?>" placeholder="Votre prénom">
            <div class="invalid-feedback"><?= $errors['firstname'] ?></div>
        </div>
        <div class="form-group">
            <label>Nom</label>
            <input type="text" name="lastname" value="<?= $values['lastname'] ?>" class="form-control <?= (isset($errors['lastname'])) ? 'is-invalid' : '' ?>" placeholder="Votre nom">
            <div class="invalid-feedback"><?= $errors['lastname'] ?></div>
        </div>
        <div class="form-group">
            <label>Addresse mail ou pseudo</label>
            <input type="text" name="email" value="<?= $values['email'] ?>" class="form-control <?= (isset($errors['email'])) ? 'is-invalid' : '' ?>" placeholder="Votre adresse mail ou pseudo">
            <div class="invalid-feedback"><?= $errors['email'] ?></div>
        </div>
        <div class="form-group">
            <label>Mot de passe</label>
            <input type="password" name="password" class="form-control <?= (isset($errors['password'])) ? 'is-invalid' : '' ?>" placeholder="Votre mot de passe">
            <div class="invalid-feedback"><?= $errors['password'] ?></div>
        </div>
        <div class="form-group">
            <label>Confirmation du mot de passe</label>
            <input type="password" name="password_repeated" class="form-control" placeholder="Votre mot de passe">
        </div>
        <button class="btn btn-primary mr-3" type="submit">S'inscrire</button>
        <a href="./login">Se connecter</a>
    </form>
</div>

<?php require_once 'parts/footer.php'; ?>