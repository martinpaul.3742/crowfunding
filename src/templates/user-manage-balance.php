<?php require_once 'parts/header.php'; ?>

<div class="container">
    <h1 class="text-center">Gestion du portefeuille</h1>

    <form method="POST" class="my-3">
        <div class="form-group">
            <p class="mb-4">Montant enregistré : <?= $u->getBalance() ?>€</p>
            <label>Montant à ajouter</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">€</span>
                </div>
                <input type="number" name="new_balance" class="form-control">
                <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Enregistrer le montant</button>
    </form>
</div>

<?php require_once 'parts/footer.php'; ?>