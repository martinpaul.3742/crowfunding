<?php require_once 'parts/header.php'; ?>

<!-- CSS -->
<link rel="stylesheet" href="css/project-show.css">

<div class="container">
    <h1><?= $project->getName() ?></h1>
    <p class="mb-4"><?= $project->getDateCreation()->format('d-m-Y') ?></p>
    <p><?= $project->getDescription() ?></p>
    <p class="author">Par <a href="./profile?user_id=<?= $project->getUserId() ?>"><?= $project->getAuthor()->getFullname() ?></a></p>
    <hr>

    <?php if ($project->getUserId() !== $u->getId()) { ?>

        <a href="./gift-new?project_id=<?= $project->getId() ?>" class="btn btn-success">Faire un don</a>

    <?php } else { ?>

        <a href="./project-edit?project_id=<?= $project->getId() ?>" class="btn btn-success">Modifier</a>

    <?php } ?>

    <?php
    $gifts = $project->getGifts();

    if ($gifts) {
    ?>

        <h2 class="mt-5">Dons effectués</h2>

        <div class="container-card-gift">

            <?php foreach ($gifts as $gift) { ?>

                <div class="card-gift d-flex">
                    <p class="date m-0"><?= $gift->getDateCreation()->format('d-m-Y') ?></p>
                    <div>
                        <p class="amount m-0"><?= $gift->getAmount() ?>€</p>
                        <p class="name m-0">par <a href="./profile?user_id=<?= $gift->getAuthor()->getId() ?>"><?= $gift->getAuthor()->getFullname() ?></a></p>
                    </div>
                </div>

            <?php } ?>

        </div>

    <?php } ?>
</div>

<?php require_once 'parts/footer.php'; ?>