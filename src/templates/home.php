<?php require_once 'parts/header.php'; ?>

<style>
    .pictures {
        height: calc(100vh - 125px - 280px);
        position: relative;
    }
    .pictures .picture {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: center;
    }
</style>

<div class="container mb-3">

    <div class="pictures card mb-4">
        <div class="picture" style="background-image:url('medias/home-background.jpg');"></div>
    </div>

    <h1 class="mb-4">Derniers projets</h1>

    <div class="row mb-5">
        <?php foreach ($projects as $project) {
            echo '<div class="col-4">';
            include('parts/card-project.php');
            echo '</div>';
        } ?>
    </div>

    <?php include('parts/pagination.php'); ?>

    <p class="text-center"><a href="./new-data">Nouvelles données</a></p>
</div>


<?php require_once 'parts/footer.php'; ?>