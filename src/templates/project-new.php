<?php require_once 'parts/header.php'; ?>

<div class="container">
    <h1 class="text-center">Nouveau projet</h1>

    <form method="POST" class="my-3">
        <div class="form-group">
            <label>Nom</label>
            <input type="text" name="name" value="<?= $values['name'] ?>" class="form-control <?= (isset($errors['name'])) ? 'is-invalid' : '' ?>" placeholder="Le nom de votre projet">
            <div class="invalid-feedback"><?= $errors['name'] ?></div>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control <?= (isset($errors['description'])) ? 'is-invalid' : '' ?>" placeholder="Decrivez votre projet"><?= $values['description'] ?></textarea>
            <div class="invalid-feedback"><?= $errors['description'] ?></div>
        </div>
        <div class="form-group">
            <label>Date limite</label>
            <input type="date" name="deadline" value="<?= $values['deadline'] ?>" class="form-control <?= (isset($errors['deadline'])) ? 'is-invalid' : '' ?>">
            <div class="invalid-feedback"><?= $errors['deadline'] ?></div>
        </div>
        <div class="form-group">
            <label>Objectif (en euros)</label>
            <div class="input-group mb-3  <?= (isset($errors['goal'])) ? 'is-invalid' : '' ?>">
                <div class="input-group-prepend">
                    <span class="input-group-text">€</span>
                </div>
                <input type="number" name="goal" value="<?= $values['goal'] ?>" class="form-control">
                <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                </div>
            </div>
            <div class="invalid-feedback"><?= $errors['goal'] ?></div>
        </div>
        <button class="btn btn-primary" type="submit">Enregistrer le projet</button>
    </form>
</div>

<?php require_once 'parts/footer.php'; ?>