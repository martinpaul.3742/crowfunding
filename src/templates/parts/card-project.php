<?php $auhtor = $project->getAuthor(); ?>

<div class="card card-project mb-4 text-center <?php echo ($project->getSumOfGifts() == $project->getGoal()) ? 'goal' : ''; ?>">
    <div class="card-header">
        <p class="m-0"><?= $project->getName() ?></p>
    </div>
    <div class="card-body">
        <p class="goal d-flex justify-content-center align-items-start"><?= $project->getSumOfGifts() ?>€<span>/<?= $project->getGoal() ?>€</span></p>
        <p class="description"><a href="./project-show?project_id=<?= $project->getId() ?>"><?= str_split($project->getDescription(), 70)[0] ?>...</a></p>
        <hr>
        <p class="date mb-4">
            Créé le <?= $project->getDateCreation()->format('d-m-Y') ?> à <?= $project->getDateCreation()->format('H:i') ?>h
            <br/>
            par <a href="./profile?user_id=<?= $auhtor->getId() ?>" class="author"><?= $auhtor->getFullname() ?></a>
        </p>
        <?php if ($u !== null) { ?>
            <?php if ($project->getUserId() !== $u->getId()) { ?>
                <p class="make-gift m-0"><a href="./gift-new?project_id=<?= $project->getId() ?>" class="btn btn-success">Faire un don</a></p>
            <?php } else { ?>
                <p class="make-gift m-0"><a href="./project-show?project_id=<?= $project->getId() ?>" class="btn btn-primary">Voir mon projet</a></p>
            <?php } ?>
        <?php } else { ?>
                <p class="make-gift m-0"><a href="./gift-new?project_id=<?= $project->getId() ?>" class="btn btn-success">Faire un don</a></p>
        <?php } ?>
    </div>
</div>