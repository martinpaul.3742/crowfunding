<div class="card card-add-project mb-4">
    <div class="card-body text-center">
        <i class="bi bi-plus-circle-fill mb-2"></i>
        <p class="card-title m-0">Ajouter un projet</p>
        <a href="./project-new"></a>
    </div>
</div>