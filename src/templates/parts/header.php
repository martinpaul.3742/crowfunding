<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crowfunding</title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-icons.css">
    <link rel="stylesheet" href="css/base.css">

</head>
<body>

<header class="mb-5">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <h4><a class="site-title navbar-brand" href="./"><i class="bi bi-award-fill mr-2"></i><span>Crowfunding</span></a></h4>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
    
            <div class="collapse navbar-collapse jusify-content-end flex-grow-0" id="navbarColor02">
                <?php if ($u !== null) { ?>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $u->getFirstname() . ' ' . $u->getLastname() ?></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="./my-profile">Mon profil</a>
                                <a class="dropdown-item" href="./project-new">Nouveau projet</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="./manage-balance">Gérer mon argent</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="./logout">Déconnexion</a>
                            </div>
                        </li>
                    </ul>
                <?php } else { ?>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item mr-3">
                            <a class="nav-link" href="./login">Connexion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn btn-success text-white" href="./register">Inscription</a>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </nav>
</header>