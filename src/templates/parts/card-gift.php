<div class="card-gift d-flex">
    <p class="date m-0"><?php echo $gift->getDateCreation()->format('d-m-Y') ?></p>
    <div>
        <p class="amount m-0"><?= $gift->getAmount() ?>€</p>
        <p class="name m-0"><a href="./project-show?project_id=<?= $gift->getProject()->getId() ?>"><?= $gift->getProject()->getName() ?></a></p>
    </div>
</div>
