<?php
    // vérifie le nombres max de liens à afficher (le premier, dernier et courant seront toujours affichés)
    $numberLinks = $pagination['maxlinks'];
    $numberLinks = ($numberLinks === null) ? 5 : $numberLinks;
    $numberLinks = ($numberLinks > ($pagination['limit'])) ? $pagination['limit'] : $numberLinks;

    $index = 0;
    $indexNext = 1;
    $indexPrevious = 1;
    $links = [
        '1' => '<a href="?page=1">1</a>',
        $pagination['limit'] => '<a href="?page=' . $pagination['limit'] . '"><span>' . $pagination['limit'] . '</span></a>',
        $pagination['current'] => '<a href="#0" class="current"><span class="d-flex justify-content-center">' . $pagination['current'] . '</span></a>'
    ];

    // rajoute les liens autour de la page courante
    while (count($links) < $numberLinks) {

        if ($index % 2 == 0) {
            $page = $pagination['current'] + $indexNext;
            $indexNext++;
        } else {
            $page = $pagination['current'] - $indexPrevious;
            $indexPrevious++;
        }

        if ($links[$page] === null && $page > 1 && $page < $pagination['limit']) {
            $links[$page] = '<a href="?page=' . $page . '"><span>' . $page . '</span></a>';
        }

        $index++;
    }

    // génère l'html
    if ($pagination !== null) {
        echo '<div class="pagination d-flex justify-content-between mb-5">';
            echo ($pagination['previous']) ? '<a href="?page=' . $pagination['previous'] . '" class="previous">Précédent</a>' : '<span></span>';
            echo '<div class="d-flex justify-content-center">';
                for ($i = 1; $i <= $pagination['limit']; $i++) {

                    $previousLink = $links[$i - 1];
                    $currentLink = $links[$i];

                    if ($currentLink !== null ) {
                        echo $currentLink;
                    } else if ($currentLink === null && $previousLink !== null) {
                        echo '<span>...</span>';
                    }
                }
            echo '</div>';
            echo ($pagination['next']) ? '<a href="?page=' . $pagination['next'] . '" class="next">Suivant</a>' : '<span></span>';
        echo '</div>';
    }
?>