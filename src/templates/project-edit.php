<?php require_once 'parts/header.php'; ?>

<div class="container">
    <h1 class="text-center"><span>Modification du projet :</span><br/><?= $project->getName() ?></h1>

    <form method="POST" class="my-3">
        <div class="form-group">
            <label>Nom</label>
            <input type="text" name="name" value="<?= $values['name'] ?>" class="form-control <?= (isset($errors['name'])) ? 'is-invalid' : '' ?>" placeholder="Le nom de votre projet">
            <div class="invalid-feedback"><?= $errors['name'] ?></div>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control <?= (isset($errors['description'])) ? 'is-invalid' : '' ?>" placeholder="Decrivez votre projet"><?= $values['description'] ?></textarea>
            <div class="invalid-feedback"><?= $errors['description'] ?></div>
        </div>
        <button class="btn btn-primary" type="submit">Enregistrer les modifications</button>
    </form>
</div>

<?php require_once 'parts/footer.php'; ?>