<?php require_once 'parts/header.php'; ?>

<div class="container text-center">
    <h1 class="mb-4">Merci pour votre générosité !</h1>
    <div class="alert alert-dismissible alert-success">
        Votre don a bien été enregistré pour le projet : "<strong><?= $project->getName() ?></strong>"<br/><br/><a href="./" class="alert-link">Retour à l'accueil</a>
    </div>
</div>

<?php require_once 'parts/footer.php'; ?>