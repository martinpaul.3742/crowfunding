<?php

require_once 'User.php';
require_once 'src/Database.php';

class Project
{
    private $id;
    private $userId;
    private $name;
    private $description;
    private $dateCreation;
    private $deadline;
    private $goal;

    public function __construct($project = []) {
        if (count($project) > 0) {
            $this->id = $project['id'];
            $this->userId = $project['user_id'];
            $this->name = $project['name'];
            $this->description = $project['description'];
            $this->goal = $project['goal'];
            $this->password = $project['password'];
            $this->deadline = $project['deadline'];
            $this->dateCreation = new DateTime($project['date_creation']);
        }
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of dateCreation
     */ 
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set the value of dateCreation
     *
     * @return  self
     */ 
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get the value of deadline
     */ 
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set the value of deadline
     *
     * @return  self
     */ 
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get the value of goal
     */ 
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set the value of goal
     *
     * @return  self
     */ 
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get the author
     *
     * @return  self
     */ 
    public function getAuthor()
    {
        $values = Database::selectBy('user', ['*'], [['id', '=', $this->getUserId()]])[0];
        return new User($values);
    }

    /**
     * Get the sum of all gifts
     *
     * @return  self
     */ 
    public function getGifts()
    {
        $gifts = [];
        $gifts_values = Database::selectBy('gift', ['*'], [['project_id', '=', $this->id]], ['date_creation' => 'DESC']);

        foreach ($gifts_values as $gv) {
            $gifts[] = new Gift($gv);
        }

        return $gifts;
    }

    /**
     * Get the sum of all gifts
     *
     * @return  self
     */ 
    public function getSumOfGifts()
    {
        $sum = 0;
        $gifts_values = Database::selectBy('gift', ['amount'], [['project_id', '=', $this->id]]);

        foreach ($gifts_values as $gv) {
            $sum += $gv['amount'];
        }

        return $sum;
    }

    /**
     * Get the sum of all gifts
     *
     * @return  self
     */ 
    public function getRest()
    {
        return ($this->goal - $this->getSumOfGifts());
    }
}