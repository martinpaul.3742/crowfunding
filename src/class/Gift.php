<?php

require_once 'Project.php';
require_once 'src/Database.php';

class Gift
{
    private $id;
    private $userId;
    private $projectId;
    private $amount;
    private $dateCreation;

    public function __construct($gift = []) {
        if (count($gift) > 0) {
            $this->id = $gift['id'];
            $this->userId = $gift['user_id'];
            $this->projectId = $gift['project_id'];
            $this->amount = $gift['amount'];
            $this->dateCreation = new DateTime($gift['date_creation']);
        }
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of projectId
     */ 
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set the value of projectId
     *
     * @return  self
     */ 
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get the value of amount
     */ 
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the value of amount
     *
     * @return  self
     */ 
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of dateCreation
     */ 
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set the value of dateCreation
     *
     * @return  self
     */ 
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get the project
     *
     * @return  self
     */ 
    public function getProject()
    {
        $values = Database::selectBy('project', ['*'], [['id', '=', $this->getProjectId()]])[0];
        return new Project($values);
    }

    /**
     * Get the author
     *
     * @return  self
     */ 
    public function getAuthor()
    {
        $values = Database::selectBy('user', ['*'], [['id', '=', $this->getUserId()]])[0];
        return new User($values);
    }
}