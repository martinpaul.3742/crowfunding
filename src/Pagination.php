<?php

class Pagination
{
    /**
     * Permet de créer une pagination
     * 
     * @param string $table 
     * @param array $order
     * @param int $numberPerPage
     * @return array
     */
    public static function paginate($table = 'project', $order = ['date_creation', 'DESC'], $numberPerPage = 10)
    {
        $page = $_GET['page'];

        if ($page === null) {
            $page = 1;
        }

        if (is_int($page + 0)) {
            $number = Database::count($table);
            $first = ($page - 1) * $numberPerPage;
            $results = self::getResults($table, $order, $first, $numberPerPage);

            $previous = ($page == 1) ? false : ($page - 1);
            $next = ($page * $numberPerPage >= $number) ? false : ($page + 1);
            $limit = ceil($number / $numberPerPage);

            return [
                'results' => $results,
                'current' => $page,
                'previous' => $previous,
                'next' => $next,
                'limit' => $limit
            ];
        }
    }


    private static function getResults($table, $order, $first, $numberPerPage)
    {
        $pdo = Database::getInstance();
        $sql = 'SELECT * FROM ' . $table . ' ORDER BY ' . $order[0] . ' ' . $order[1] . ' LIMIT ' . $first . ', ' . $numberPerPage;

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $stmt = $stmt->fetchAll();

        return $stmt;
    }
}
