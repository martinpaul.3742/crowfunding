<?php

class Database
{
    private static $pdo = null;
    private static $dsn = null;
    private static $user = null;
    private static $password = null;

    private function __construct()
    {
        //
    }

    /**
     * Permet de créer une instance de PDO
     *
     * @param string $dsn
     * @param string $user
     * @param string $assword
     * @return PDO $pdo
     */
    public static function getInstance() {

        if (self::$dsn === null || self::$user === null || self::$password === null) {
            $param = parse_ini_file("./config.ini");
            self::$dsn = $param['dsn'];
            self::$user = $param['user'];
            self::$password = $param['password'];

        }

        if (self::$pdo === null) {
            try {
                self::$pdo = new PDO(self::$dsn, self::$user, self::$password);
            } catch (PDOException $e) {
                throw new Exception('Connexion échouée : ' . $e->getMessage());
            }
        }

        return self::$pdo;
    }

    /**
     * Selectionne les infos d'une table en base de données
     *
     * @param string $table
     * @param array $parameters
     * @param array $order (['date_creation' => 'DESC'])
     * @return array | false
     */
    public static function select($table = 'project', $parameters = ['*'], $order = false)
    {
        $pdo = self::getInstance();

        $sql = 'SELECT ';
        for ($i = 0; $i < count($parameters); $i++) {
            $parameter = $parameters[$i];
            $sql .= ($i == count($parameters) - 1) ? $parameter : $parameter . ', ';
        }
        $sql .= ' FROM ' . $table;

        if ($order) {
            $sql .= ' ORDER BY ';
            $i = 0;
            foreach ($order as $key => $o) {
                $sql .= ($i == count($order) - 1) ? $key . ' ' . $o : $key . ' ' . $o . ', ';
                $i++;
            }
        }

        $stmt = $pdo->prepare($sql);
        $stmt->execute(); 

        $stmt = $stmt->fetchAll();

        return (count($stmt) == 0) ? false : $stmt;
    }

    /**
     * Selectionne les infos d'une table à l'aide de paramètres
     *
     * @param string $table
     * @param array $parameters
     * @param array $values ([['firstName', '=', 'toto'], ['lastName', '=', 'titi']], ...)
     * @param array $order (['date_creation' => 'DESC'])
     * @return array | false
     */
    public static function selectBy($table = 'project', $parameters = ['*'], $values, $order = false)
    {
        $pdo = self::getInstance();
        $valuesForStatement = [];

        $sql = 'SELECT ';
        for ($i = 0; $i < count($parameters); $i++) {
            $parameter = $parameters[$i];
            $sql .= ($i == count($parameters) - 1) ? $parameter : $parameter . ', ';
        }
        $sql .= ' FROM ' . $table . ' WHERE ';

        for ($i = 0; $i < count($values); $i++) {
            $value = $values[$i];
            $sql .= $value[0] . ' ' . $value[1] . ' :' . $value[0] . ' ';
            $sql = ($i == count($values) - 1) ? $sql : $sql . ' AND ';
            $valuesForStatement[':'.$value[0]] = $value[2];
        }

        if ($order) {
            $sql .= ' ORDER BY ';
            $i = 0;
            foreach ($order as $key => $o) {
                $sql .= ($i == count($order) - 1) ? $key . ' ' . $o : $key . ' ' . $o . ', ';
                $i++;
            }
        }

        $stmt = $pdo->prepare($sql);
        $stmt->execute($valuesForStatement);
        $stmt = $stmt->fetchAll(); 

        return (count($stmt) == 0) ? false : $stmt;
    }

    /**
     * Insère en base de données, dans une table, une nouvelle colonne
     *
     * @param string $table
     * @param array $values
     * @return void
     */
    public static function insert($table = 'project', $values = ['*'])
    {
        $pdo = self::getInstance();
        $valuesForStatement = [];

        $sql = 'INSERT INTO ' . $table . ' (';
        $i = 0;
        foreach ($values as $key => $value) {
            $sql .= ($i == count($values) - 1) ? $key . ') VALUES (' : $key . ', ';
            $i++;
        }

        $i = 0;
        foreach ($values as $key => $value) {
            $sql .= ($i == count($values) - 1) ? ':' . $key . ')' : ':' . $key . ', ';
            $valuesForStatement[':' . $key] = $values[$key];
            $i++;
        }

        $stmt = $pdo->prepare($sql);
        $stmt->execute($valuesForStatement);
    }


    /**
     * En base de données, met à jour les données d'une colonne dans une table
     *
     * @param string $table
     * @param int $id id de la colonne
     * @param array $values (['firstName' => 'toto', 'lastName' => 'titi', ...])
     * @return void
     */
    public static function update($table, $id, $values)
    {
        $pdo = self::getInstance();
        $valuesForStatement = [
            ':id' => $id
        ];

        $sql = 'UPDATE ' . $table . ' SET ';
        $i = 0;
        foreach ($values as $key => $value) {
            $sql .= ($i == count($values) - 1) ? $key . ' = :' . $key . ' WHERE id = :id' : $key . ' = :' . $key . ', ';
            $valuesForStatement[':' . $key] = $values[$key];
            $i++;
        }

        $stmt = $pdo->prepare($sql);
        $stmt->execute($valuesForStatement);
    }

    /**
     * En base de données, supprime la colonne d'une table
     *
     * @param string $table
     * @param array $id id de la colonne
     * @return void
     */
    public static function delete($table, $id)
    {
        $pdo = self::getInstance();
        
        $sql = 'DELETE FROM ' . $table . ' WHERE id = ' . $id;

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

    /**
     * Compte le nombre de colonnes d'une table
     *
     * @param string $table
     * @return int
     */
    public static function count($table)
    {
        $pdo = self::getInstance();        
        $sql = 'SELECT COUNT(*) AS number FROM ' . $table;

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $stmt = $stmt->fetch();

        return $stmt['number'];
    }
}