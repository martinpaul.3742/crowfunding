<?php

class HomeController extends AbstractController
{
    public static function index()
    {
        $pagination = Pagination::paginate('project', ['date_creation', 'DESC'], 9); // on créé une pagination pour les projets
        $pagination['maxlinks'] = 4;

        $projects_results = $pagination['results'];
        $projects = [];
        foreach ($projects_results as $pr) { // instancie les projets
            $projects[] = new Project($pr);
        }

        return self::render('home.php', [
            'projects' => $projects,
            'pagination' => $pagination
        ]);
    }
}