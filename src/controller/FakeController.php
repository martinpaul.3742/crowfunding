<?php

require_once 'AbstractController.php';

class FakeController extends AbstractController
{
    /**
     * Regénère des nouvelles données
     *
     * @return void
     */
    public static function newData()
    {
        // vide la base de données
        self::emptyDatabase();

        // génère les nouveaux utilisateurs
        self::setUsers();

        // génère l'utilisateur par défaut
        self::defaultUser();

        // génère les nouveaux projets
        self::setProjects();
        
        // génère les nouveaux dons
        self::setGifts();


        // test si un projet a reçu plus de don que nécessaire
        // $test = Database::select('project', ['*']);
        // foreach ($test as $t) {
        //     $p = new Project($t);
        //     if ($p->getSumOfGifts() > $p->getGoal()) {
        //         var_dump($p);
        //     }
        // }

        // connexion automatique
        $me = Database::selectBy('user', ['*'], [['email', '=', 'test@gmail.com']])[0];
        $_SESSION['user'] = $me['email'];

        return self::redirectToPath('./');
    }
    
    public static function setUsers()
    {
        $users = [];
        $usersResponse = [];
        $femaleUrl = 'https://fakerapi.it/api/v1/persons?_quantity=20&_gender=female&_birthday_start=2005-01-01';
        $maleUrl = 'https://fakerapi.it/api/v1/persons?_quantity=20&_gender=male&_birthday_start=2005-01-01';


        $opts = array('https' => ['method'  => 'GET']);
        $context = stream_context_create($opts); 

        // get values for females
        $results = file_get_contents($femaleUrl, false, $context);
        $femalesResponse = json_decode($results, true)['data'];
        foreach ($femalesResponse as $u) { $usersResponse[] = $u; };

        // get values for males
        $results = file_get_contents($maleUrl, false, $context);
        $malesResponse = json_decode($results, true)['data'];
        foreach ($malesResponse as $u) { $usersResponse[] = $u; };

        // insère les valeurs
        $index = 0;
        foreach ($usersResponse as $userResponse) {
            $users[] = [
                'pseudo' => strtolower($userResponse['firstname']) . '_' . $index,
                'firstname' => $userResponse['firstname'],
                'lastname' => $userResponse['lastname'],
                'email' => $userResponse['email'],
                'password' => password_hash('password', PASSWORD_ARGON2I),
                'balance' => rand(20, 300),
                'date_registration' => self::randomDate('-3 years', 'now')
            ];
            $index++;
        }
        
        foreach ($users as $user) {
            Database::insert('user', $user);
        }
    }

    public static function setProjects()
    {
        $users = Database::select('user', ['*']);
        $projects = [];
        $url = 'https://fakerapi.it/api/v1/texts?_quantity=50&_characters=500';
        $opts = array('https' => ['method'  => 'GET']);
        $context = stream_context_create($opts); 

        // projects
        $results = file_get_contents($url, false, $context);
        $projectsResponse = json_decode($results, true)['data'];

        // insère les valeurs
        foreach ($projectsResponse as $projectResponse) {
            $user = $users[rand(0, count($users) - 1)];
            $projects[] = [
                'name' => $projectResponse['title'],
                'description' => $projectResponse['content'],
                'date_creation' => self::randomDate((new DateTime($user['date_registration']))->format('Y-m-d H:i:s'), 'now'),
                'deadline' => self::randomDate('now', '+2 years'),
                'goal' => rand(20, 500),
                'user_id' => $user['id']
            ];
        }

        foreach ($projects as $project) {
            Database::insert('project', $project);
        }
    }

    private static function setGifts()
    {
        $usersValues = Database::select('user', ['*']);   
        $projectsValues = Database::select('project', ['*']);
        $projects = [];

        // instancie les projets pour accéder aux méthodes
        foreach ($projectsValues as $pv) {
            $projects[] = new Project($pv);
        }

        for ($i = 0; $i < 120; $i++) {
            // project
            $project = $projects[rand(0, count($projects) - 1)];
            while ($project->getRest() == 0) {
                $project = $projects[rand(0, count($projects) - 1)];
            }

            // user
            $user = $usersValues[rand(0, count($usersValues) - 1)];

            // date
            $dateRegistrationAuthor = (new DateTime($project->getAuthor()->getDateRegistration()));
            $dateRegistrationUser = (new DateTime($user['date_registration']));

            if ($dateRegistrationAuthor > $dateRegistrationUser) {
                $date = self::randomDate($dateRegistrationAuthor->format('Y-m-d H:i:s'), 'now');
            } else {
                $date = self::randomDate($dateRegistrationUser->format('Y-m-d H:i:s'), 'now');
            }

            // insère les valeurs en base de données
            Database::insert('gift', [
                'user_id' => $user['id'],
                'project_id' => $project->getId(),
                'amount' => rand(1, $project->getRest()),
                'date_creation' => $date,
            ]);
        }
    }

    private static function emptyDatabase()
    {
        $users = Database::select('user', ['id']);
        $projects = Database::select('project', ['id']);
        $gifts = Database::select('gift', ['id']);

        foreach ($gifts as $gift) {
            Database::delete('gift', $gift['id']);
        }

        foreach ($projects as $project) {
            Database::delete('project', $project['id']);
        }

        foreach ($users as $user) {
            Database::delete('user', $user['id']);
        }

    }

    private static function defaultUser()
    {
        $usersValues = Database::select('user', ['*']);
        $userRandom = $usersValues[rand(0, count($usersValues) - 1)];
        Database::update('user', $userRandom['id'], [
            'email' => 'test@gmail.com',
            'password' => password_hash('password', PASSWORD_ARGON2I),
        ]);
    }

    /**
     * Method to generate random date between two dates
     * @param $sStartDate
     * @param $sEndDate
     * @param string $sFormat
     * @return bool|string
     */
    private static function randomDate($sStartDate, $sEndDate, $sFormat = 'Y-m-d H:i:s')
    {
        // Convert the supplied date to timestamp
        $fMin = strtotime($sStartDate);
        $fMax = strtotime($sEndDate);

        // Generate a random number from the start and end dates
        $fVal = mt_rand($fMin, $fMax);

        // Convert back to the specified date format
        return date($sFormat, $fVal);
    }
}