<?php

include_once '../../Route.php';

abstract class AbstractController
{
    /**
     * Récupère l'utilisateur connecté
     *
     * @return User()
     */
    public static function user()
    {
        $userValue = Database::selectBy('user', '*', [['email', '=', $_SESSION['user']]])[0];

        return new User($userValue);
    }

    /**
     * Redirige l'utilisateur
     *
     * @param string $path
     * @return void
     */
    public static function redirectToPath($path)
    {
        header('Location: ' . $path); 
    }

    /**
     * Permet d'associer des données à une "Vue" (template)
     *
     * @param string $pathView
     * @param array $data
     * @return void
     */
    public static function render($pathView, $data = [])
    {
        $pathView = 'src/templates/' . $pathView;

        if ($_SESSION['user']) {
            $data['u'] = self::user();
        }

        ob_start();
        extract($data);
        require($pathView);
    
        return $pathView;
    }
}