<?php

class GiftController extends AbstractController
{
    public static function new()
    {
        $projectId = $_GET['project_id'];

        if ($projectId == null) {
            self::redirectToPath('./');
        } else {
            $values = Database::selectBy('project', ['*'], [['id', '=', $projectId]])[0];
            $project = new Project($values);
        }

        if (self::user() === null) { // si l'utilisateur n'est pas connecté on le redirige sur la page de connexion puis sur cette page
            $_SESSION['redirect'] = './gift-new?project_id=' . $projectId;
            self::redirectToPath('./login');
        }

        if (!$values || self::user()->getId() == $project->getUserId()) { // si le projet existe pas en bdd ou si il appartient à l'utilisateur connecté on redirige sur la page d'accueil
            self::redirectToPath('./');
        }

        $verify = self::verifyFormNew($project);
        $values = $verify['values'];
        $errors = $verify['errors'];

        if ($verify && count($errors) == 0) {
            $values['user_id'] = self::user()->getId();
            $values['project_id'] = $project->getId();
            $values['date_creation'] = (new DateTime('now'))->format('Y-m-d H:i:s');
            Database::insert('gift', $values);

            $newBalance = self::user()->getBalance() - $values['amount'];
            self::user()->setBalance($newBalance); // on met à jour l'objet User connecté
            Database::update('user', self::user()->getId(), ['balance' => $newBalance]); // on met à jour l'utilisateur en base de données

            return self::success($project);
        }

        return self::render('gift-new.php', [
            'project' => $project,
            'user' => $project->getAuthor(),
            'errors' => $errors,
            'values' => $values
        ]);
    }

    private static function success($project)
    {
        return self::render('gift-success.php', [
            'project' => $project
        ]);
    }

    /**
     * Vérifie les données d'un form d'un nouveau don
     */
    private static function verifyFormNew($project)
    {
        $values = [];
        $errors = [];

        if (isset($_POST['amount'])) {

            // amount
            if ($_POST['amount'] == '') {
                $errors['amount'] = 'Veuillez entrer un montant';
            } else if ($_POST['amount'] < 1) {
                $errors['amount'] = 'Veuillez entrer un montant valide';
            } else if (self::user()->getBalance() < $_POST['amount']) {
                $errors['amount'] = 'Vous ne disposez pas assez d\'argent pour effectuer ce don';
            } else if ($_POST['amount'] > $project->getRest()) {
                $errors['amount'] = 'Le don maximal pour ce projet est de ' . $project->getRest() . '€';
            } else {
                $values['amount'] = $_POST['amount'];
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }
}