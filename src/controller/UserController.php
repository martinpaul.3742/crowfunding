<?php

class UserController extends AbstractController
{
    public static function myProfile()
    {
        $user = self::user();

        // form edit informations
        $values = [
            'pseudo' => $user->getPseudo(),
            'firstname' => $user->getFirstName(),
            'lastname' => $user->getLastName(),
            'email' => $user->getEmail()
        ];

        // change informations
        $verify = self::verifyFormInformations();
        if ($verify) {
            $values = $verify['values'];
            $errors = $verify['errors'];

            if (count($errors) == 0) {
                Database::update('user', $user->getId(), $values);
                $user = Database::selectBy('user', ['*'], [['id', '=', $user->getId()]])[0];
                $_SESSION['user'] = $user['email'];
            }
        }

        // form edit password
        $verify = self::verifyFormPassword();
        $errorsPassword = $verify['errors'];

        // change password
        if ($verify) {
            $values = $verify['values'];
            $values['password'] = password_hash($values['password'], PASSWORD_ARGON2I);

            if (count($errorsPassword) == 0) {
                Database::update('user', $user->getId(), $values);
                $user = Database::selectBy('user', ['*'], [['id', '=', $user->getId()]])[0];
                $_SESSION['user'] = $user['email'];
            }
        }

        // get projects
        $projects_results = Database::selectBy('project', ['*'], [['user_id', '=', self::user()->getId()]], ['date_creation' => 'DESC']);
        $projects = [];
        foreach ($projects_results as $pr) {
            $projects[] = new Project($pr);
        }

        // get gifts
        $gifts_results = Database::selectBy('gift', ['*'], [['user_id', '=', self::user()->getId()]], ['date_creation' => 'DESC']);
        $gifts = [];
        foreach ($gifts_results as $gr) {
            $gifts[] = new Gift($gr);
        }
        
        return self::render('user-my-profile.php', [
            'values' => $values,
            'errors' => $errors,
            'errorsPassword' => $errorsPassword,
            'projects' => $projects,
            'gifts' => $gifts
        ]);
    }

    public static function profile()
    {
        $userId = $_GET['user_id'];

        if ($userId == null) {
            self::redirectToPath('./');
        } else if (self::user() !== null) { // si l'id est celui de l'utilisateur connecté, on redirige sur son profil personnel
            if ($userId == self::user()->getId()) {
                self::redirectToPath('./my-profile');
            }
        }
        
        $user = Database::selectBy('user', ['*'], [['id', '=', $userId]])[0];

        if (!$user) { // si l'utlisateur n'existe pas en base de données on redirige sur la page d'accueil
            self::redirectToPath('./');
        }

        // get projects
        $projects_results = Database::selectBy('project', ['*'], [['user_id', '=', $userId]], ['date_creation' => 'DESC']);
        $projects = [];
        foreach ($projects_results as $pr) { // on instancie les projets
            $projects[] = new Project($pr);
        }

        return self::render('user-profile.php', [
            'projects' => $projects,
            'user' => new User($user)
        ]);
    }

    public static function manageBalance()
    {
        if (isset($_POST['new_balance'])) {
            $newBalance = self::user()->getBalance() + $_POST['new_balance'];
            Database::update('user', self::user()->getId(), ['balance' => $newBalance]);
            self::user()->setBalance($newBalance);
        }

        return self::render('user-manage-balance.php');
    }

    public static function login()
    {
        $errors = [];

        if (isset($_POST['email']) && isset($_POST['password'])) {

            if (count($errors) == 0) {
                $user = Database::selectBy('user', ['*'], [['email', '=', $_POST['email']]])[0];
                $user = ($user == false) ? Database::selectBy('user', ['*'], [['pseudo', '=', $_POST['email']]])[0] : $user; // on vérifie pour l'email ET le pseudo

                if ($user && password_verify($_POST['password'], $user['password'])) {
                    $_SESSION['user'] = $user['email'];

                    if ($_SESSION['redirect']) { // s'il l'utilisateur souhaitait accéder à une page on redirige dessus
                        self::redirectToPath($_SESSION['redirect']);
                    } else {
                        self::redirectToPath('./');
                    }
                } else {
                    $errors['global'] = 'L\'adresse mail et le mot de passe ne correspondent pas';
                }
            }
        }

        return self::render('login.php', [
            'errors' => $errors
        ]);
    }

    public static function logout()
    {
        session_unset();
        self::redirectToPath('./');
    }

    public static function new()
    {
        $verify = self::verifyFormRegister();
        $values = $verify['values'];
        $errors = $verify['errors'];
        
        if ($verify && count($errors) == 0) {
            // register
            $values['password'] = password_hash($values['password'], PASSWORD_ARGON2I);
            $values['date_registration'] = (new DateTime('now'))->format('Y-m-d H:i:s');
            $values['balance'] = 40;
            Database::insert('user', $values);
            // auto login
            $user = Database::selectBy('user', ['*'], [['email', '=', $_POST['email']]])[0];
            if ($user) {
                $_SESSION['user'] = $user['email'];
                self::redirectToPath('./');
            }
        }

        self::render('user-new.php', [
            'errors' => $errors,
            'values' => $values
        ]);
    }

    /**
     * Vérifie les données d'un form d'inscription'
     */
    private static function verifyFormRegister()
    {
        $values = [];
        $errors = [];
        $items = ['pseudo', 'firstname', 'lastname', 'email', 'password', 'password_repeated'];

        if (isset($_POST['pseudo']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_repeated'])) {

            // secure
            foreach ($items as $item) {
                $_POST[$item] = htmlspecialchars($_POST[$item]);
            }

            // pseudo
            if ($_POST['pseudo'] == '') {
                $errors['pseudo'] = 'Veuillez entrer un pseudo';
            } else {
                $values['pseudo'] = $_POST['pseudo'];
            }

            // firstname
            if ($_POST['firstname'] == '') {
                $errors['firstname'] = 'Veuillez entrer un pénom';
            } else {
                $values['firstname'] = $_POST['firstname'];
            }

            // firstname
            if ($_POST['lastname'] == '') {
                $errors['lastname'] = 'Veuillez entrer un nom';
            } else {
                $values['lastname'] = $_POST['lastname'];
            }

            // email
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Veuillez entrer une adresse mail valide';
            } else if (Database::selectBy('user', ['*'], [['email', '=', $_POST['email']]])) {
                $errors['email'] = 'Cette adresse mail est déjà enregistrée';
            } else {
                $values['email'] = $_POST['email'];
            }

            // password
            if ($_POST['password'] !== $_POST['password_repeated']) {
                $errors['password'] = 'Les mots de passe ne correspondent pas';
            } else if (!filter_var($_POST['password'], FILTER_VALIDATE_REGEXP, ['options'=> ['regexp' => '/.{6,25}/']])) {
                $errors['password'] = 'Votre mot de passe doit contenir entre 6 et 25 caractères';
            } else {
                $values['password'] = $_POST['password'];
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }

    /**
     * Vérifie les données d'un form de modification de profil
     */
    private static function verifyFormInformations()
    {
        $values = [];
        $errors = [];
        $items = ['pseudo', 'firstname', 'lastname', 'email'];

        
        if (isset($_POST['pseudo']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email'])) {
            
            // secure
            foreach ($items as $item) {
                $_POST[$item] = htmlspecialchars($_POST[$item]);
            }

            // pseudo
            if ($_POST['pseudo'] == '') {
                $errors['pseudo'] = 'Veuillez entrer un pseudo';
            } else {
                $values['pseudo'] = $_POST['pseudo'];
            }
            
            // firstname
            if ($_POST['firstname'] == '') {
                $errors['firstname'] = 'Veuillez entrer un pénom';
            } else {
                $values['firstname'] = $_POST['firstname'];
            }

            // firstname
            if ($_POST['lastname'] == '') {
                $errors['lastname'] = 'Veuillez entrer un nom';
            } else {
                $values['lastname'] = $_POST['lastname'];
            }

            // email
            $existEmail = Database::selectBy('user', ['*'], [['email', '=', $_POST['email']]])[0]; // Si l'email existe déjà
            $existEmail = ($existEmail === null || $existEmail['id'] == self::user()->getId()) ? false : true; // Et si ce n'est pas celui de l'utilisateur
            
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Veuillez entrer une adresse mail valide';
            } else if ($existEmail) {
                $errors['email'] = 'Cette adresse mail est déjà enregistrée';
            } else {
                $values['email'] = $_POST['email'];
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }

    /**
     * Vérifie les données d'un form de modification de mot de passe
     */
    private static function verifyFormPassword()
    {
        $values = [];
        $errors = [];
        $items = ['password_old', 'password_new', 'password_new_repeated'];

        if (isset($_POST['password_old']) && isset($_POST['password_new']) && isset($_POST['password_new_repeated'])) {

            // secure
            foreach ($items as $item) {
                $_POST[$item] = htmlspecialchars($_POST[$item]);
            }

            // password_old
            if (!password_verify($_POST['password_old'], self::user()->getPassword())) {
                $errors['password_old'] = 'Votre ancien mot de passe est incorrect';
            }
            
            // password_new
            else if ($_POST['password_new'] !== $_POST['password_new_repeated']) {
                $errors['password_new'] = 'Les mots de passe ne correspondent pas';
            } else if (!filter_var($_POST['password_new'], FILTER_VALIDATE_REGEXP, ['options'=> ['regexp' => '/.{6,25}/']])) {
                $errors['password_new'] = 'Votre mot de passe doit contenir entre 6 et 25 caractères';
            } else if ($_POST['password_old'] == $_POST['password_new']) {
                $errors['password_new'] = 'Veuillez choisir un mot de passe différent de l\'existant';
            } else {
                $values['password'] = htmlspecialchars($_POST['password_new']);
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }
}