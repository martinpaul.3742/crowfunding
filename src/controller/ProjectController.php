<?php

class ProjectController extends AbstractController
{
    public static function show()
    {
        $projectId = $_GET['project_id'];

        if ($projectId == null) {
            return self::redirectToPath('./');
        } else {
            $values = Database::selectBy('project', ['*'], [['id', '=', $projectId]])[0];
        }
        
        if ($values) {
            self::render('project-show.php', [
                'project' => new Project($values)
            ]);
        } else {
            return self::redirectToPath('./');
        }
    }

    public static function new()
    {
        $verify = self::verifyFormNew();
        $values = $verify['values'];
        $errors = $verify['errors'];

        if ($verify && count($errors) == 0) {
            $values['user_id'] = self::user()->getId();
            $values['date_creation'] = (new DateTime('now'))->format('Y-m-d H:i:s');
            Database::insert('project', $values);
            $projects = Database::selectBy('project', ['*'], [['user_id', '=', self::user()->getId()]], ['date_creation' => 'DESC']);
            $projectId = $projects[0]['id'];
            self::redirectToPath('./project-show?project_id=' . $projectId);
        }

        self::render('project-new.php', [
            'values' => $values,
            'errors' => $errors
        ]);
    }

    public static function edit()
    {
        $projectId = $_GET['project_id'];

        if ($projectId == null) {
            return self::redirectToPath('./');
        } else {
            $values = Database::selectBy('project', ['*'], [['id', '=', $projectId]])[0];
            $project = new Project($values);
        }
        
        if ($project->getUserId() !== self::user()->getId()) { // si le projet n'appartient pas à l'utilisateur connecté, on redirige sur la page d'accueil
            return self::redirectToPath('./');
        }
        
        $verify = self::verifyFormEdit();

        if ($verify) {
            $values = $verify['values'];
            $errors = $verify['errors'];
        }

        if ($verify && count($errors) == 0 && $project->getUserId() == self::user()->getId()) {
            Database::update('project', $projectId, $values);
            self::redirectToPath('./project-show?project_id=' . $projectId);
        }

        self::render('project-edit.php', [
            'project' => $project,
            'values' => $values,
            'errors' => $errors
        ]);
    }

    public static function delete()
    {
        $projectId = $_GET['project_id'];

        if ($projectId == null) {
            return self::redirectToPath('./');
        } else {
            $values = Database::selectBy('project', ['*'], [['id', '=', $projectId]])[0];
            $project = new Project($values);
        }
        
        if ($project->getUserId() !== self::user()->getId()) { // si le projet n'appartient pas à l'utilisateur connecté, on redirige sur la page d'accueil
            return self::redirectToPath('./');
        }

        Database::delete('project', $projectId);

        self::render('my-profile.php');
    }

    /**
     * Vérifie les données d'un form d'un nouveau projet
     */
    private static function verifyFormNew()
    {
        $values = [];
        $errors = [];
        $items = ['name', 'description', 'goal', 'deadline'];

        if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['goal']) && isset($_POST['deadline'])) {

            // secure
            foreach ($items as $item) {
                $_POST[$item] = htmlspecialchars($_POST[$item]);
            }

            // name
            if ($_POST['name'] == '') {
                $errors['name'] = 'Veuillez entrer un nom de projet';
            } else {
                $values['name'] = $_POST['name'];
            }

            // description
            if ($_POST['description'] == '') {
                $errors['description'] = 'Veuillez entrer une description';
            } else if (strlen($_POST['description']) >= 200 || strlen($_POST['description']) <= 20) {
                $errors['description'] = 'La description doit avoir entre 20 et 200 caractères';
            } else {
                $values['description'] = $_POST['description'];
            }

            // goal
            if ($_POST['goal'] == '') {
                $errors['goal'] = 'Veuillez entrer un montant';
            } else {
                $values['goal'] = $_POST['goal'];
            }

            // deadline
            if ($_POST['deadline'] == '') {
                $errors['deadline'] = 'Veuillez entrer une date limite';
            } else if (!(new DateTime('now') < (new DateTime($_POST['deadline'])))) {
                $errors['deadline'] = 'La date doit être supérieure à aujourd\'hui';
            } else {
                $values['deadline'] = $_POST['deadline'];
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }

    /**
     * Vérifie les données d'un form de modification de projet
     */
    private static function verifyFormEdit()
    {
        $values = [];
        $errors = [];
        $items = ['name', 'description'];

        if (isset($_POST['name']) && isset($_POST['description'])) {

            // secure
            foreach ($items as $item) {
                $_POST[$item] = htmlspecialchars($_POST[$item]);
            }

            // name
            if ($_POST['name'] == '') {
                $errors['name'] = 'Veuillez entrer un nom de projet';
            } else {
                $values['name'] = $_POST['name'];
            }

            // description
            if ($_POST['description'] == '') {
                $errors['description'] = 'Veuillez entrer une description';
            } else if (strlen($_POST['description']) >= 200 || strlen($_POST['description']) <= 20) {
                $errors['description'] = 'La description doit avoir entre 20 et 200 caractères';
            } else {
                $values['description'] = $_POST['description'];
            }

            return ['values' => $values, 'errors' => $errors];
        } else {
            return false;
        }
    }
}