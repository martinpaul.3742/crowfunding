<?php

// Class
require_once 'src/class/User.php';
require_once 'src/class/Project.php';
require_once 'src/class/Gift.php';

session_start();

require_once 'src/Database.php';
require_once 'src/Pagination.php';

// Controller
require_once 'src/controller/AbstractController.php';
require_once 'src/controller/HomeController.php';
require_once 'src/controller/UserController.php';
require_once 'src/controller/ProjectController.php';
require_once 'src/controller/GiftController.php';
require_once 'src/controller/FakeController.php';

// Include router
include('Route.php');


// Base route
Route::add('/', function() { HomeController::index(); });

// User
Route::add('/register', function() { UserController::new(); });
Route::add('/register', function() { UserController::new(); }, 'post');
Route::add('/login', function() { UserController::login(); });
Route::add('/login', function() { UserController::login(); }, 'post');
Route::add('/logout', function() { UserController::logout(); });
Route::add('/profile', function() { UserController::profile(); });
Route::add('/my-profile', function() { UserController::myProfile(); });
Route::add('/my-profile', function() { UserController::myProfile(); }, 'post');
Route::add('/manage-balance', function() { UserController::manageBalance(); });
Route::add('/manage-balance', function() { UserController::manageBalance(); }, 'post');

// Projects
Route::add('/project-show', function() { ProjectController::show(); });
Route::add('/project-new', function() { ProjectController::new(); });
Route::add('/project-new', function() { ProjectController::new(); }, 'post');
Route::add('/project-edit', function() { ProjectController::edit(); });
Route::add('/project-edit', function() { ProjectController::edit(); }, 'post');

// Gifts
Route::add('/gift-new', function() { GiftController::new(); });
Route::add('/gift-new', function() { GiftController::new(); }, 'post');

// Fake
Route::add('/new-data', function() { FakeController::newData(); });

Route::run('/');
