## Configurer le fichier config.ini 
- Paramétrez les infos de connexion à la bdd (dsn, user et password)
- Paramétrez path : si le projet est dans un sous dossier par rapport à la racine du serveur mettre le chemin du répértoire du projet depuis la racine (ex : "/projets-cours/crowfunding/") sinon mettre "/"
## Créer la bdd avec le fichier bdd.sql
Importer bdd.sql dans phpmyadmin
## Une fois sur la page d'accueil cliquer tout en bas sur "Nouvelles données"
L'identifiant et mdp par défaut :
- test@gmail.com
- password

Enjoy 🙂 🐙
