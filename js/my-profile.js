let buttonUpdateProfile = $('#button-update-profile'),
    buttonCancelUpdateProfile = $('#button-cancel-update-profile'),
    buttonUpdatePassword = $('#button-update-password'),
    buttonCancelUpdatePassword = $('#button-cancel-update-password'),
    containerUpdateProfile = $('#update-profile'),
    containerUpdateInformations = $('#update-informations'),
    containerUpdatePassword = $('#update-password');


function showUpdateProfile() {
    $(containerUpdateProfile).show();
    $(containerUpdateInformations).show();
    $(buttonUpdatePassword).show();
    $(containerUpdatePassword).hide();
    $(buttonCancelUpdatePassword).hide();
}

function showUpdatePassword() {
    $(buttonUpdatePassword).hide();
    $(containerUpdateInformations).hide();
    $(containerUpdatePassword).show();
    $(buttonCancelUpdatePassword).show();
}

$(buttonUpdateProfile).on('click', showUpdateProfile);
$(buttonUpdatePassword).on('click', showUpdatePassword);

$(buttonCancelUpdateProfile).on('click', function() {
    $(containerUpdateProfile).hide();
})

$(buttonCancelUpdatePassword).on('click', function() {
    $(containerUpdateProfile).hide();
    $(containerUpdateInformations).show();
    $(containerUpdatePassword).hide();
    $(buttonCancelUpdatePassword).hide();
})


let url = window.location.href,
    anchor = url.substring(url.indexOf("#")+1);

if (anchor == 'update-profile') {
    showUpdateProfile();
} else if (anchor == 'update-password') {
    showUpdateProfile();
    showUpdatePassword();
}